#!/usr/bin/python3
""" Alta3 Research | rzfeeser@alta3.com
   opening a static file containing JSON data and transforming
   it into a format that Python understands"""

# JSON is part of the Python Standard Library
import json

def main():
    """runtime code"""
    ## open the file
    with open("firewall.json", "r") as datacenter:
        datacenterstring = datacenter.read()

    ## display our decoded string
    print(datacenterstring)
    print(type(datacenterstring))           
    print("\nThe code above is string data. Python cannot easily work with this data.")
    input("Press Enter to continue\n")            

    ## Create the JSON string
    datacenterdecoded = json.loads(datacenterstring)

    ## This is now a dictionary
    print(type(datacenterdecoded))

    ## display the IP in the datacenter
    print(datacenterdecoded)

    ## display the IP in blocked
    print(datacenterdecoded["blocked"][-1])

    ## display the 2nd IP in secondary
    print(datacenterdecoded["secondary"][1])

    ## write code to
    ## display the last server in blocked

if __name__ == "__main__":
    main()

