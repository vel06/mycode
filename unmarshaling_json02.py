#!/usr/bin/python3
""" Alta3 Research | rzfeeser@alta3.com    
Opening a static file containing JSON data and
transforming it into a Pythonic format"""

# JSON is part of the Python Standard Library
import json

def main():
    """runtime code"""
    ## open the file
    with open("firewall.json", "r") as datacenter:
        datacenterdecoded = json.load(datacenter)

    ## This is now a dictionary
    print(type(datacenterdecoded))

    ## display the servers in the datacenter
    print(datacenterdecoded)

    ## display the servers in blocked
    print(datacenterdecoded["blocked"])

    ## display the 2nd server in secondary
    print(datacenterdecoded["secondary"][1])

if __name__ == "__main__":
    main()

