#!/usr/bin/python3
""" Alta3 Research | RZFeeser@alta3.com
   The json.dumps() function creates a JSON string. You could
   use this string to attach to a HTTP response."""

# JSON is part of the Python Standard Library
import json

def main():
    """runtime code"""
    ## create a blob of data to work with
    marvel_characters = [{"name": "Cyclops", "power": "energy weapon (eyes)"},
      {"name": "storm", "power": "control weather"}]

    ## display our Python data (a list containing two dictionaries)
    print(marvel_characters)

    ## Create the JSON string
    jsonstring = json.dumps(marvel_characters)

    ## Display a single string of JSON
    print(jsonstring)

if __name__ == "__main__":
    main()

